<?php

use
    Sabre\DAV;
use Sabre\DAV\Auth;
use Sabre\DAV\PropertyStorage;
// The autoloader
require 'vendor/autoload.php';

// Serving Directory
$rootDirectory = new DAV\FS\Directory('/drupal-data');

// The server object is responsible for making sense out of the WebDAV protocol
$server = new DAV\Server($rootDirectory);

// Endpoint to access webdav, TODO: Discuss path
$server->setBaseUri('/_webdav');

// Don't add Lock!!
// It will cause 0 byte files, see https://sabre.io/dav/0bytes/

// This ensures that we get a pretty index in the browser, but it is
// optional.
$server->addPlugin(new DAV\Browser\Plugin());


// This ensures auth is enabled
$authBackend = new Auth\Backend\File('htdigest/htdigest');
$authBackend->setRealm('SabreDAV');
$authPlugin = new Auth\Plugin($authBackend);
$server->addPlugin($authPlugin);


// Others?
$server->addPlugin(new \Sabre\DAV\Mount\Plugin());


// All we need to do now, is to fire up the server
$server->exec();

?>
