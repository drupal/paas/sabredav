ARG PHP_VERSION
FROM php:${PHP_VERSION}

# PHP FPM
RUN set -eux; \
	\
	apk add --no-cache --virtual .build-deps autoconf g++ make \
		coreutils \
		freetype-dev \
		libjpeg-turbo-dev \
		libpng-dev \
		libzip-dev \
		mysql-client \
	; \
    \
    rm -rf /tmp/pear \
    ; \
	\
	docker-php-ext-configure gd \
		--with-freetype-dir=/usr/include \
		--with-jpeg-dir=/usr/include \
		--with-png-dir=/usr/include \
	; \
	\
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		zip \
	; \
	\
	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)"; \
	apk add --virtual .drupal-phpexts-rundeps $runDeps; \
	apk del .build-deps

# - php-fpm
# Easiest way to set php.ini config
## These config files contain the limit POST request php will accept
## when uploading to WebDav through web browser
ADD ./php-fpm/config/zz-docker.conf /usr/local/etc/php/conf.d/config.ini
ADD ./php-fpm/config/www.conf /usr/local/etc/php-fpm.d/www.conf

# Temporary, remove once fully tested with serving PV
RUN mkdir /webdav

COPY . /webdav
WORKDIR /webdav
