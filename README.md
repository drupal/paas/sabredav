# SabreDav

This project is based on https://github.com/sabre-io/dav    
Goal: Give webdav support to Drupal websites


# Plugins 🧩

## Auth

Enabling this plugin allows to protect the webdav access with a login.    
It may be possible to integrate more complex Auth, see: https://sabre.io/dav/authentication/    
The current implementation is using a `htdigest` file, from class `Sabre\DAV\Auth\Backend\File`.   
The expected path to have the password stored is `htdigest/htdigest`.


## Browser

This plugin is responsible for generating HTML for browser access.

## Lock

This plugin ensures users don't overwrite each others changes.

## Mount

This plugin allows to downlad a DAVMOUNT file to mount the filesystem externally, to
retrieve the mount file, add `/?mount` in the end of the path.   
